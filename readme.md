### Steps to run

- Run docker-compose.yml with __docker-compose up -d__.
- Install dependencies by composer with __composer install__
- Copy .env.example to .env
- Set mysql credendials, use __bionexo__ on (db|user|pass) in .env file
- run __php artisan key:generate__
- run __php artisan config:cache__
- run migrate __php artisan migrate__
- Seeds "especialidades"  __php artisan db:seed__
- run API __php artisan serve__