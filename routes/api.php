<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('doctors')->group(function () {
    Route::post('/', 'DoctorRegisterController@register');
    Route::get('/', 'DoctorListController@list');
    Route::get('/{id}', 'DoctorSeeController@see');
    Route::put('/{id}', 'DoctorUpdateController@update');
    Route::delete('/{id}', 'DoctorDeleteController@delete');

});
Route::get('/expertises', 'ExpertisesListController@list');