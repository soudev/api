<?php

namespace App\Models\Expertises;

use Illuminate\Database\Eloquent\Model;

class Expertises extends Model
{
    /**
     * @var string
     */
    protected $table = 'expertises';
    /**
     * @var array
     */
    protected $fillable = [
        'expertise'
    ];

}
