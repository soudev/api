<?php

namespace App\Models\DoctorExpertises;


use App\Models\Doctor\DoctorFactory;

/**
 * Class DoctorExpertisesRegister
 * @package App\Models\DoctorExpertises
 */
class DoctorExpertisesRegister
{

    /**
     * @var
     */
    private $doctorExpertisesFactory;

    /**
     * DoctorExpertisesRegister constructor.
     */
    public function __construct(DoctorExpertisesFactory $doctorExpertisesFactory)
    {
        $this->doctorExpertisesFactory = $doctorExpertisesFactory;
    }

    /**
     * @param DoctorFactory $doctor
     * @param $request
     */
    public function register(DoctorFactory $doctor, $request){

            foreach (array_unique($request['expertises']) as $expertise) {
                $this->doctorExpertisesFactory::create([
                    'doctor_id' => $doctor->id,
                    'expertises_id' => $expertise
                ]);
            }
    }
}