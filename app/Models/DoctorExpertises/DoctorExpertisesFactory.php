<?php

namespace App\Models\DoctorExpertises;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DoctorExpertisesFactory
 * @package App\Models\DoctorExpertises
 */
class DoctorExpertisesFactory extends Model
{
    /**
     * @var string
     */
    protected $table = 'doctor_expertises';
    /**
     * @var array
     */
    protected $fillable = [
        'doctor_id' , 'expertises_id'
    ];
}
