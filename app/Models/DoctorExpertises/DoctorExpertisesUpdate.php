<?php

namespace App\Models\DoctorExpertises;


use App\Models\Doctor\DoctorFactory;

/**
 * Class DoctorExpertisesUpdate
 * @package App\Models\DoctorExpertises
 */
class DoctorExpertisesUpdate
{

    /**
     * @var
     */
    private $doctorExpertisesFactory;

    /**
     * DoctorExpertisesUpdate constructor.
     */
    public function __construct(DoctorExpertisesFactory $doctorExpertisesFactory)
    {
        $this->doctorExpertisesFactory = $doctorExpertisesFactory;
    }

    /**
     * @param DoctorFactory $doctor
     * @param $request
     */
    public function update(DoctorFactory $doctor, $request){

            $this->doctorExpertisesFactory->where('doctor_id', $request['id'])->delete();
            foreach (array_unique($request['expertises']) as $expertise) {
                $this->doctorExpertisesFactory::create([
                    'doctor_id' => $request['id'],
                    'expertises_id' => $expertise
                ]);
            }
    }
}