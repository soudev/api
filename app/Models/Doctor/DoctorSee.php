<?php

namespace App\Models\Doctor;

/**
 * Class DoctorSee
 * @package App\Models\Doctor
 */
class DoctorSee
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorCreate constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }


    /**
     * @return DoctorFactory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function see($id)
    {
        return $doctor = $this->doctor->with('expertises')->find($id);
    }

}