<?php

namespace App\Models\Doctor;

/**
 * Class DoctorDelete
 * @package App\Models\Doctor
 */
class DoctorDelete
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorCreate constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->doctor  = $this->doctor->find($id);
        $this->doctor->delete();
    }

}