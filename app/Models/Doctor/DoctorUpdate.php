<?php

namespace App\Models\Doctor;

/**
 * Class DoctorUpdate
 * @package App\Models\Doctor
 */
class DoctorUpdate
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorCreate constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @param $request
     * @return bool
     */
    public function update($id, $request)
    {

        $this->doctor  = $this->doctor->find($id);
        $this->doctor->fill($request);
        return $this->doctor->save();

    }

}