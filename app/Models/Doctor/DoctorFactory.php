<?php

namespace App\Models\Doctor;

use App\Models\Expertises\Expertises;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Doctor
 * @package App\Models\Doctor
 */
class DoctorFactory extends Model
{

    /**
     * @var string
     */
    protected $table = 'doctors';
    /**
     * @var array
     */
    protected $fillable = [
        'name' , 'crm' , 'phone'
    ];

    /**
     * @return mixed
     */
    public function expertises(){
        return $this->belongsToMany(Expertises::class, 'doctor_expertises', 'doctor_id')->select(['expertise']);
    }

    /**
     *
     */
    public function withExpertises(){
        foreach ($this->expertises as $expertise){
            $this->expertises[] = $expertise->expertise;
        }


    }
}

