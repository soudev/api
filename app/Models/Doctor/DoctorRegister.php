<?php

namespace App\Models\Doctor;

/**
 * Class DoctorRegister
 * @package App\Models\Doctor
 */
class DoctorRegister
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorCreate constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @param $request
     * @return bool
     */
    public function register($request)
    {

        $this->doctor->fill($request);
        $this->doctor->save();

    }

}