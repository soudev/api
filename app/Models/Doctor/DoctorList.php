<?php

namespace App\Models\Doctor;

/**
 * Class DoctorList
 * @package App\Models\Doctor
 */
class DoctorList
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorCreate constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }


    /**
     * @return DoctorFactory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function list()
    {
        return $this->doctor->all();
    }

}