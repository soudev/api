<?php

namespace App\Models\Doctor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class DoctorValidate
 * @package App\Models\Doctor
 */
class DoctorValidate
{


    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var array
     */
    private $rules = [
            'name' => 'required|max:255',
            'crm' => 'required|max:20',
            'phone' => 'max:19',
            'expertises' => 'required|array|min:2'
        ];
    /**
     * @var array
     */
    private $messages = [
        'name.required' => 'O nome é obrigatório',
        'name.max' => 'O tamanho máximo para o nome é 255 caracteres',
        'crm.required' => 'O CRM é obrigatório',
        'crm.unique' => 'O CRM já está cadastrado',
        'crm.unique' => 'O CRM já está cadastrado',
        'crm.max' => 'O CRM jnão pode ser maior que 20',
        'phone.max' => 'O tamanho máximo para o telefone é 19 caracteres',
        'expertises.required' => 'Envie no minímo duas especialidades',
        'expertises.max' => 'Envie no minímo duas especialidades',

    ];

    /**
     * DoctorValidate constructor.
     */
    public function __construct(Validator $validator, Request $request)
    {
        $this->validator = $validator;
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator()
    {
        return $this->validator::make($this->request->all(), $this->rules, $this->messages);
    }


}