<?php

namespace App;

class ApiResponse
{
    public static function response($message, $code = 201)
    {
        return response()->json(
            ['data' => [
                'msg' => $message,
                'code' => $code,
                'time' => time()
            ]], $code);
    }
}