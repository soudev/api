<?php

namespace App\Http\Controllers;

use App\ApiError;
use App\ApiResponse;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorList;

/**
 * Class DoctorListController
 * @package App\Http\Controllers
 */
class DoctorListController extends Controller
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorListController constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        try {
            $doctor_list = new DoctorList($this->doctor);
            return ApiResponse::response($doctor_list->list());
        }catch (\Exception $e){
            return ApiError::errorMessage($e->getMessage(), 400);
        }
    }

}
