<?php

namespace App\Http\Controllers;

use App\ApiError;
use App\ApiResponse;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorRegister;
use App\Models\Doctor\DoctorUpdate;
use App\Models\Doctor\DoctorValidate;
use App\Models\DoctorExpertises\DoctorExpertisesFactory;
use App\Models\DoctorExpertises\DoctorExpertisesRegister;
use App\Models\DoctorExpertises\DoctorExpertisesUpdate;
use Illuminate\Http\Request;

/**
 * Class DoctorUpdateController
 * @package App\Http\Controllers
 */
class DoctorUpdateController extends Controller
{
    /**
     * @var Doctor
     */
    private $doctor;
    /**
     * @var DoctorExpertisesFactory
     */
    private $expertises;
    /**
     * @var DoctorValidate
     */
    private $validate;

    /**
     * DoctorUpdateController constructor.
     * @param DoctorFactory $doctor
     * @param DoctorExpertisesFactory $expertises
     * @param DoctorValidate $validate
     */
    public function __construct(DoctorFactory $doctor, DoctorExpertisesFactory $expertises, DoctorValidate $validate)
    {
        $this->doctor = $doctor;
        $this->expertises = $expertises;
        $this->validate = $validate;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

        try {

            $doctorData = $request->all();

            $validator = $this->validate->validator($doctorData);
            if ($validator->fails()) {
                return ApiError::errorMessage($validator->errors(), 422);
            }

            $update = new DoctorUpdate($this->doctor);
            $update->update($id, $doctorData);

            $expertises = new DoctorExpertisesUpdate($this->expertises);
            $expertises->update($this->doctor, array_merge(['id'=>$id], $doctorData));

            $message = 'Médico atualizado com sucesso!';
            return ApiResponse::response($message);

        } catch (\Exception $e) {
            return ApiError::errorMessage($e->getMessage(), 400);
        }
    }
}
