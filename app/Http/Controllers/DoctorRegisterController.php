<?php

namespace App\Http\Controllers;

use App\ApiError;
use App\ApiResponse;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorRegister;
use App\Models\Doctor\DoctorValidate;
use App\Models\DoctorExpertises\DoctorExpertisesFactory;
use App\Models\DoctorExpertises\DoctorExpertisesRegister;
use Illuminate\Http\Request;

/**
 * Class DoctorRegisterController
 * @package App\Http\Controllers
 */
class DoctorRegisterController extends Controller
{
    /**
     * @var Doctor
     */
    private $doctor;
    /**
     * @var DoctorExpertisesFactory
     */
    private $expertises;
    /**
     * @var DoctorValidate
     */
    private $validate;

    /**
     * DoctorRegisterController constructor.
     * @param DoctorFactory $doctor
     * @param DoctorExpertisesFactory $expertises
     * @param DoctorValidate $validate
     */
    public function __construct(DoctorFactory $doctor, DoctorExpertisesFactory $expertises, DoctorValidate $validate)
    {
        $this->doctor = $doctor;
        $this->expertises = $expertises;
        $this->validate = $validate;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        try {

            $doctorData = $request->all();

            $validator = $this->validate->validator($doctorData);
            if ($validator->fails()) {
                return ApiError::errorMessage($validator->errors(), 422);
            }

            $register = new DoctorRegister($this->doctor);
            $register->register($doctorData);

            $expertises = new DoctorExpertisesRegister($this->expertises);
            $expertises->register($this->doctor, $doctorData);

            $message = 'Médico criado com sucesso!';
            return ApiResponse::response($message);

        } catch (\Exception $e) {
            return ApiError::errorMessage($e->getMessage(), 400);
        }
    }
}
