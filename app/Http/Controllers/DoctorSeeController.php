<?php

namespace App\Http\Controllers;

use App\ApiError;
use App\ApiResponse;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorSee;

/**
 * Class DoctorSeeController
 * @package App\Http\Controllers
 */
class DoctorSeeController extends Controller
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * DoctorSeeController constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function see($id)
    {
        try {
            $doctor_see = new DoctorSee($this->doctor);
            return ApiResponse::response($doctor_see->see($id));
        }catch (\Exception $e){
            return ApiError::errorMessage($e->getMessage(), 400);
        }
    }

}
