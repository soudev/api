<?php

namespace App\Http\Controllers;

use App\ApiError;
use App\ApiResponse;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorRegister;
use App\Models\Doctor\DoctorDelete;
use App\Models\Doctor\DoctorValidate;
use App\Models\DoctorExpertises\DoctorExpertisesFactory;
use App\Models\DoctorExpertises\DoctorExpertisesRegister;
use App\Models\DoctorExpertises\DoctorExpertisesUpdate;
use Illuminate\Http\Request;

/**
 * Class DoctorDeleteController
 * @package App\Http\Controllers
 */
class DoctorDeleteController extends Controller
{
    /**
     * @var Doctor
     */
    private $doctor;
    /**
     * @var DoctorExpertisesFactory
     */
    private $expertises;
    /**
     * @var DoctorValidate
     */
    private $validate;

    /**
     * DoctorDeleteController constructor.
     * @param DoctorFactory $doctor
     */
    public function __construct(DoctorFactory $doctor)
    {
        $this->doctor = $doctor;
    }


    /**
     * @param Doctor $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {

            $delete = new DoctorDelete($this->doctor);
            $delete->delete($id);
            $message = 'Médico removido com sucesso!';
            return ApiResponse::response($message, 200);
        } catch (\Exception $e) {
            return ApiError::errorMessage($e->getMessage(), 400);
        }
    }
}
