<?php

namespace App\Http\Controllers;

use App\ApiError;
use App\ApiResponse;
use App\Models\Expertises\Expertises;

/**
/**
 * Class ExpertisesListController
 * @package App\Http\Controllers
 */
class ExpertisesListController extends Controller
{
    /**
     * @var Doctor
     */
    private $expertises;

    /**
     * ExpertisesListController constructor.
     * @param Expertises $doctor
     */
    public function __construct(Expertises $expertises)
    {
        $this->expertises = $expertises;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        try {
            $expertises = new Expertises;
            return ApiResponse::response($expertises->all());
        }catch (\Exception $e){
            return ApiError::errorMessage($e->getMessage(), 400);
        }
    }

}
