<?php

namespace App;

class ApiError
{
    public static function errorMessage($message, $code = 500)
    {
        return response()->json(
            ['data' => [
                'msg' => $message,
                'code' => $code,
                'time' => time()
            ]], $code);
    }
}