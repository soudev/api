<?php

use Illuminate\Database\Seeder;

class ExpertisesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expertises')->insert([
            [
                'expertise' => 'ALERGOLOGIA',
            ],
            [
                'expertise' => 'ANGIOLOGIA',
            ],
            [
                'expertise' => 'BUCO MAXILO',
            ],
            [
                'expertise' => 'CARDIOLOGIA CLÍNICA',
            ],
            [
                'expertise' => 'CARDIOLOGIA INFANTIL',
            ],
            [
                'expertise' => 'CARDIOLOGIA INFANTIL',
            ],
            [
                'expertise' => 'CIRURGIA CARDÍACA',
            ],
            [
                'expertise' => 'CIRURGIA DE CABEÇA/PESCOÇO',
            ],
            [
                'expertise' => 'CIRURGIA DE TÓRAX',
            ],
            [
                'expertise' => 'CIRURGIA GERAL',
            ],
            [
                'expertise' => 'CIRURGIA PEDIÁTRICA',
            ],
            [
                'expertise' => 'CIRURGIA PLÁSTICA',
            ],
            [
                'expertise' => 'CIRURGIA TORÁCICA',
            ],
            [
                'expertise' => 'CIRURGIA VASCULAR',
            ],
            [
                'expertise' => 'CLÍNICA MÉDICA',
            ],
        ]);
    }
}
