<?php

namespace Tests\Unit;

use App\Models\Doctor\DoctorDelete;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorRegister;
use App\Models\Doctor\DoctorSee;
use App\Models\Doctor\DoctorUpdate;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


/**
 * Class DoctorTest
 * @package Tests\Unit
 */
class DoctorTest extends TestCase
{

    use DatabaseMigrations;
    /**
     * @var
     */
    private $model;


    public function setUp(): void
    {
        parent::setUp();
        $this->model = new DoctorFactory();
    }


    public function testObjectType()
    {
        $this->assertInstanceOf("app\Models\Doctor\DoctorFactory", $this->model);
    }


    public function testRegisterDoctor()
    {

        $register = new DoctorRegister($this->model);
        $register->register($this->doctorData());

        $this->assertInstanceOf("app\Models\Doctor\DoctorRegister", $register);

        $this->assertDatabaseHas('doctors', $this->doctorData());

    }

    /**
     * @depends testRegisterDoctor
     */

    public function testFindDoctor()
    {

        $register = new DoctorRegister($this->model);
        $register->register($this->doctorData());

        $doctor = new DoctorSee($this->model);
        $doctor = $doctor->see(1);

        $this->assertEquals(1, $doctor->id);
        $this->assertEquals($this->doctorData()['name'], $doctor->name);
        $this->assertEquals($this->doctorData()['crm'], $doctor->crm);
        $this->assertEquals($this->doctorData()['phone'], $doctor->phone);
    }


    public function testEditDoctor()
    {
        $register = new DoctorRegister($this->model);
        $register->register($this->doctorData());

        $doctor = new DoctorSee($this->model);
        $doctor = $doctor->see(1);

        $data_update = [
            'name' => 'Teste Update',
            'crm' => '12345',
            'phone' => '987654321',
        ];

        $doctor_update = new DoctorUpdate($this->model);

        $doctor_update->update($doctor->id, $data_update);

        $this->assertDatabaseHas('doctors', [
            'name' => 'Teste Update',
            'crm' => '12345',
            'phone' => '987654321',
        ]);
    }

    public function testDeleteDoctor()
    {
        $register = new DoctorRegister($this->model);
        $register->register($this->doctorData());

        $doctor = new DoctorSee($this->model);
        $doctor_see = $doctor->see(1);

        $doctor_delete = new DoctorDelete($this->model);
        $doctor_delete->delete($doctor_see->id);

        $doctor_see = $doctor->see(1);
        $this->assertEquals(null, $doctor_see);
    }

    /**
     * @return array
     */
    private function doctorData()
    {
        return [
            'name' => 'Teste',
            'crm' => '1234',
            'phone' => '12341234',
        ];
    }

}
