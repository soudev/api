<?php

namespace Tests\Unit;

use App\Models\Doctor\DoctorDelete;
use App\Models\Doctor\DoctorFactory;
use App\Models\Doctor\DoctorRegister;
use App\Models\Doctor\DoctorSee;
use App\Models\Doctor\DoctorUpdate;
use App\Models\Expertises\Expertises;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


/**
 * Class DoctorAPITest
 * @package Tests\Unit
 */
class DoctorAPITest extends TestCase
{

    use DatabaseMigrations;
    /**
     * @var
     */
    private $baseApi = 'http://127.0.0.1:8000/api';

    public function setUp(): void
    {
        parent::setUp();
        $expertises = new Expertises();
        $expertises->fill(['expertise' => 'ANGIOLOGIA']);
        $expertises->save();

        $expertises = new Expertises();
        $expertises->fill(['expertise' => 'CIRURGIA VASCULAR']);
        $expertises->save();
    }

    public function testRegisterDoctor()
    {
        $response = $this->call('POST', "{$this->baseApi}/doctors",
            array_merge(
                $this->doctorData(),
                ['expertises' => $this->doctorExpertisesData()]
            )
        );

        $response->assertStatus(201);
    }

    public function testRegisterDoctorError()
    {
        $response = $this->call('POST', "{$this->baseApi}/doctors", $this->doctorData());

        $response->assertStatus(422);
    }

    public function testFindDoctor()
    {

        $this->call('POST', "{$this->baseApi}/doctors",
            array_merge(
                $this->doctorData(),
                ['expertises' => $this->doctorExpertisesData()]
            )
        );

        $response = $this->call('GET', "{$this->baseApi}/doctors/1");
        $response->assertStatus(201);

    }

    public function testDeleteDoctor()
    {

        $this->call('POST', "{$this->baseApi}/doctors",
            array_merge(
                $this->doctorData(),
                ['expertises' => $this->doctorExpertisesData()]
            )
        );

        $response = $this->call('DELETE', "{$this->baseApi}/doctors/1");
        $response->assertStatus(200);

    }


    public function testDeleteDoctorError()
    {

        $this->call('POST', "{$this->baseApi}/doctors",
            array_merge(
                $this->doctorData(),
                ['expertises' => $this->doctorExpertisesData()]
            )
        );

        $response = $this->call('DELETE', "{$this->baseApi}/doctors/3");
        $response->assertStatus(500);

    }

    public function testRouteNotFound()
    {
        $response = $this->call('POST', "{$this->baseApi}/doctors/mydoctor/123", $this->doctorData());

        $response->assertStatus(404);
    }

    /**
     * @return array
     */
    private function doctorData()
    {
        return [
            'name' => 'Teste',
            'crm' => '1234',
            'phone' => '12341234',
        ];
    }

    private function doctorExpertisesData()
    {
        return [1, 2];
    }

}
